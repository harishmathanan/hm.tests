﻿using HM.Test.Models;
using System.Collections.Generic;

namespace HM.Test.Models
{
    public class ProjectArticle
    {
        public List<Project> Projects { get; set; }
        public List<Article> Articles { get; set; }
    }
}