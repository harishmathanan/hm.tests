﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HM.Test.Models
{
    public class Account
    {
        [Required]
        public string Fullname { get; set; }
        
        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}