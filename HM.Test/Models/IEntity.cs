﻿using System;

namespace HM.Test.Models
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}