﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HM.Test.Models
{
    public class Project : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(120, ErrorMessage= "Invalid length.")]
        public string Title { get; set; }

        [Required]
        public string Body { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Invalid length.")]
        public string Author { get; set; }

        public string Image { get; set; }

        [Required]
        [MaxLength(120, ErrorMessage = "Invalid length.")]
        public string FriendlyUrl { get; set; }
    }
}