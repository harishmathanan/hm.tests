﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace HM.Test.Models
{
    public class IdentityContext : IdentityDbContext<User>
    {
        public IdentityContext()
            :base("TestDb")
        {
        }
    }
}