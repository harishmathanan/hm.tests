﻿using System.Data.Entity;

namespace HM.Test.Models
{
    public class DataContext : DbContext
    {
        public DbSet<Article> Articles { get; set; }
        public DbSet<Project> Projects { get; set; }

        public DataContext()
            :base("TestDb")
        {
        }
    }
}