﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HM.Test.Models
{
    public class User : IdentityUser
    {
        [MaxLength(25, ErrorMessage= "Invalid length.")]
        public string FullName { get; set; }
    }
}