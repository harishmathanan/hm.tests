﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HM.Test.Models
{
    public class Article : IEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(120, ErrorMessage= "Invalid length.")]
        public string Title { get; set; }

        [Required]
        public string Body { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [MaxLength(50, ErrorMessage = "Invalid length.")]
        public string Author { get; set; }

        [MaxLength(50, ErrorMessage = "Invalid length.")]
        public string Tags { get; set; }

        [MaxLength(120, ErrorMessage = "Invalid length.")]
        public string FriendlyUrl { get; set; }
    }
}