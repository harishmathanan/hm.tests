﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using HM.Test.Controllers;
using System.Web.Optimization;

namespace HM.Test
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;
            
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error()
        {
            Exception exception = Server.GetLastError();
            Server.ClearError();

            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "Error");
            routeData.Values.Add("action", "Index");
            routeData.Values.Add("exception", exception);
            
            // Check if its a Web relevant error
            if (exception.GetType() == typeof(HttpException))
            {
                routeData.Values.Add("statusCode", ((HttpException)exception).GetHttpCode());
            }
            else
            {
                routeData.Values.Add("statusCode", 500);
            }

            // Avoid IIS getting in the middle
            Response.TrySkipIisCustomErrors = true;

            IController controller = new ErrorController();

            HttpContextWrapper wrapper = new HttpContextWrapper(Context);
            RequestContext request = new RequestContext(wrapper, routeData);
            controller.Execute(request);
            Response.End();
        }
    }
}
