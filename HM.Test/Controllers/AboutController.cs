﻿using System.Web.Mvc;

namespace HM.Test.Controllers
{
    public class AboutController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }
    }
}