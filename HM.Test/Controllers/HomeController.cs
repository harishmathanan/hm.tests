﻿using System;
using System.Web;
using System.Linq;
using System.Web.Mvc;
using HM.Test.Models;

namespace HM.Test.Controllers
{
    public class HomeController : Controller
    {
        private DataContext _db;

        public HomeController()
        {
            _db = new DataContext();
        }
        
        [AllowAnonymous]
        public ActionResult Index()
        {
            var viewModel = new ProjectArticle
            {
                Projects = _db.Projects.OrderByDescending(p => p.Date).Take(5).ToList(),
                Articles = _db.Articles.OrderByDescending(a => a.Date).Take(5).ToList()
            };

            return View(viewModel);
        }

        [AllowAnonymous]
        public ActionResult Test()
        {
            throw new HttpException(401, "Unauthorized action.");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}