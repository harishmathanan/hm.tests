﻿using System;
using System.Web;
using System.Linq;
using HM.Test.Models;
using System.Web.Mvc;
using System.Security.Claims;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HM.Test.Controllers
{
    public class AccountController : Controller
    {
        private UserStore<User> _userStore;
        private UserManager<User> _userManager;

        public AccountController()
        {
            _userStore = new UserStore<User>(new IdentityContext());
            _userManager = new UserManager<User>(_userStore);
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            using (var db = new IdentityContext())
            {
                if (db.Users.Count() > 0)
                {
                    throw new HttpException(403, "Sorry, but registration is closed.");
                }

                return View();
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(FormCollection collection)
        {
            if (collection["password"] != collection["confirmPassword"])
            {
                ViewBag.ErrorMessage = "Passwords do not match.";
                return View();
            }

            // Setup new User account
            var user = new User();
            user.UserName = collection["username"];
            user.FullName = collection["fullname"];

            // Create and save new account
            IdentityResult result = _userManager.Create(user, collection["password"]);

            if (result.Succeeded)
            {
                ViewBag.StatusMessage = "You have been successfully registered. Please <a href=\"/account/login\">click here</a> to login.";
                return View();
            }

            ViewBag.ErrorMessage = result.Errors.FirstOrDefault();
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            var model = new Account { ReturnUrl = returnUrl };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(FormCollection collection)
        {
            // Get the User from the store based on provide username and password
            var user = _userManager.Find(collection["username"], collection["password"]);

            // If user does not exist, or invalid credentials provided
            if (user == null)
            {
                ViewBag.ErrorMessage = "Invalid Username or Password.";
                return View();
            }

            // If user is found and verified
            var context = Request.GetOwinContext();
            var authentication = context.Authentication;

            var identity = new ClaimsIdentity(
                                                new[] { new Claim(ClaimTypes.Name, user.FullName) },
                                                DefaultAuthenticationTypes.ApplicationCookie
                                             );

            authentication.SignIn(new AuthenticationProperties() { IsPersistent = false }, identity);

            // Return user to source location
            string returnUrl = collection["returnUrl"];
            return Redirect(GetRedirectUrl(returnUrl));
        }

        public ActionResult Logout()
        {
            var context = Request.GetOwinContext();
            var authentication = context.Authentication;

            authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("index", "home");
        }

        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("index", "home");
            }

            return returnUrl;
        }
    }
}