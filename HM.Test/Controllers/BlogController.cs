﻿using System;
using System.Web;
using System.Linq;
using HM.Test.Models;
using System.Web.Mvc;
using System.Data.Entity;
using System.Collections.Generic;

namespace HM.Test.Controllers
{
    // TODO:
    // Implement paging on index action.
    public class BlogController : Controller
    {
        private DataContext _db;

        public BlogController()
        {
            // TODO:
            // Dispose of context object
            _db = new DataContext();
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            // TODO:
            // Implement paging on blog
            var articles = _db.Articles.ToList().Take(10);
            return View(articles);
        }

        [AllowAnonymous]
        public ActionResult Post(string url)
        {
            var article = _db.Articles.Where(a => a.FriendlyUrl == url).FirstOrDefault();

            if (article == null)
            {
                throw new HttpException(404, "Sorry, blog post not found.");
            }

            return View(article);
        }

        public ActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw new HttpException(401, "Unauthorized action.");
            }
            
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw new HttpException(401, "Unauthorized action.");
            }

            // Setup Article model
            var model = new Article();
            model.Title = collection["title"];
            model.Body = collection["body"];
            model.Tags = collection["tags"];
            model.FriendlyUrl = collection["friendlyUrl"];
            model.Date = DateTime.Today;
            model.Author = User.Identity.Name;

            // Save the Article
            _db.Articles.Add(model);
            _db.SaveChanges();

            return RedirectToAction("index");
        }

        public ActionResult Edit(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw new HttpException(401, "Unauthorized action.");
            }
            
            var model = _db.Articles.Find(id);
            if (model == null)
            {
                throw new HttpException(404, "Sorry, blog post not found.");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw new HttpException(401, "Unauthorized action.");
            }

            // get the object
            var model = _db.Articles.Find(Convert.ToInt32(collection["postId"]));

            // update the object
            model.Title = collection["title"];
            model.Body = collection["body"];
            model.Tags = collection["tags"];

            // save updates
            _db.Entry(model).State = EntityState.Modified;
            _db.SaveChanges();

            return RedirectToAction("index");
        }

        public ActionResult Delete(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw new HttpException(401, "Unauthorized action.");
            }
                     
            var model = _db.Articles.Find(id);
            if (model == null)
            {
                throw new HttpException(401, "Unauthorized action.");
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirm(FormCollection collection)
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw new HttpException(401, "Unauthorized action.");
            }         
                        
            var model = _db.Articles.Find(Convert.ToInt32(collection["postId"]));

            _db.Articles.Remove(model);
            _db.SaveChanges();
            
            return RedirectToAction("index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}