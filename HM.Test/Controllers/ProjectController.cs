﻿using System;
using System.Web;
using System.Linq;
using HM.Test.Models;
using System.Web.Mvc;
using System.Data.Entity;

namespace HM.Test.Controllers
{
    // TODO:
    // Implement paging on index action.
    // Implement file upload for image property.
    public class ProjectController : Controller
    {
        private DataContext _db;

        public ProjectController()
        {
            _db = new DataContext();
        }
        
        [AllowAnonymous]
        public ActionResult Index()
        {
            var projects = _db.Projects.OrderByDescending(p => p.Date).Take(10).ToList();
            return View(projects);
        }

        [AllowAnonymous]
        public ActionResult View(string url)
        {
            var project = _db.Projects.Where(p => p.FriendlyUrl == url).FirstOrDefault();

            if (project == null)
            {
                throw new HttpException(404, "Sorry, project not found.");
            }

            return View(project);
        }

        public ActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw new HttpException(401, "Unauthorized action.");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw new HttpException(401, "Unauthorized action.");
            }

            // Setup Article model
            var model = new Project();
            model.Title = collection["title"];
            model.Body = collection["body"];
            model.FriendlyUrl = collection["friendlyUrl"];
            model.Date = DateTime.Today;
            model.Author = User.Identity.Name;

            // Save the Article
            _db.Projects.Add(model);
            _db.SaveChanges();

            return RedirectToAction("index");
        }

        public ActionResult Edit(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw new HttpException(401, "Unauthorized action.");
            }            
            
            var model = _db.Projects.Find(id);
            if (model == null)
            {
                throw new HttpException(404, "Sorry, project not found.");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw new HttpException(401, "Unauthorized action.");
            }

            // get the object
            var model = _db.Projects.Find(Convert.ToInt32(collection["projectId"]));

            // update the object
            model.Title = collection["title"];
            model.Body = collection["body"];

            // save updates
            _db.Entry(model).State = EntityState.Modified;
            _db.SaveChanges();

            return RedirectToAction("index");
        }

        public ActionResult Delete(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw new HttpException(401, "Unauthorized action.");
            }    
        
            var model = _db.Projects.Find(id);
            if (model == null)
            {
                throw new HttpException(404, "Sorry, project not found.");
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirm(FormCollection collection)
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw new HttpException(401, "Unauthorized action.");
            }

            var model = _db.Projects.Find(Convert.ToInt32(collection["projectId"]));

            _db.Projects.Remove(model);
            _db.SaveChanges();

            return RedirectToAction("index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}