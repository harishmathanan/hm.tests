﻿using System;
using System.Web.Mvc;

namespace HM.Test.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index(int statusCode, Exception exception)
        {
            Response.StatusCode = statusCode;
            ViewBag.StatusCode = statusCode;

            switch (statusCode)
            {
                case 404:
                    return View("NotFound");
                case 401:
                    return View("Unauthorized");
                case 403:
                    return View("Forbidden");
                case 400:
                    return View("BadRequest");
                case 500:
                    return View("ServerError");
                default:
                    return View("Unknown", exception);
            }
        }
    }
}